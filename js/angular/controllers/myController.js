angular.module('despegar', ['ngTable', 'ngAnimate', 'toastr'])
    .controller('ctrl', ['$scope','NgTableParams','toastr', function ($scope, NgTableParams, toastr) {
        //objeto delivery para los datos del formulario 
        toastr.success('Delivery agregado correctamente!', ':)');
        $scope.delivery = {};
        $scope.save = function(form) {
            //al guardar, agrego los datos a la tabla convirtiendolos en json mediante
            $scope.tableParams.settings().dataset.push(angular.fromJson($scope.delivery));
            $scope.tableParams.reload();

          console.log(angular.fromJson($scope.delivery));
        };
     
     
        $scope.reset = function(form) {
          $scope.user = {};
          if (form) {
            form.$setPristine();
            form.$setUntouched();
          }
        };
     
     
        $scope.reset();
        $scope.idem_data = function(){
            console.log($scope.is_idem)
            if ($scope.is_idem) {
                //si hay datos en el otro form los debe copiar

            } else {
                //limpiar todos los input
            }
        }

        var dataList = [{
            idem: true,
            commercialContact: {
                email: "jsantos@gmail.com",
                name: "Juan",
                lastname: "Santos",
                phone: "45871542"
            },
            adminContact: {
                email: "jsantos@gmail.com",
                phonenumber: "45871542",
                lastname: "Santos",
                name: "Juan"
            },
            scheduleFrom: new Date(0, 0, 1, 0, 30, 0),
            scheduleTo: new Date(0, 0, 1, 0, 30, 0),
            address: "Calle 5nro 1548",
            specialties: "Parrillada",
            description: "Bar",
            phone: "45871542",
            name: "Argentino"
        },{
            idem: true,
            commercialContact: {
                email: "mbrito@gmail.com",
                name: "Mariana",
                lastname: "Brito",
                phonenumber: "45463544"
            },
            adminContact: {
                email: "aramos@gmail.com",
                phonenumber: "132356554",
                lastname: "Ramos",
                name: "Ana"
            },
            scheduleFrom: new Date(0, 0, 1, 0, 30, 0),
            scheduleTo: new Date(0, 0, 1, 0, 30, 0),
            address: "Calle 128 nro 1542",
            specialties: "Parrillada",
            description: "Bar",
            phone: "125164",
            name: "Bar BQ"
        },{
            idem: true,
            commercialContact: {
                email: "lmoreno@gmail.com",
                name: "Luis",
                lastname: "Moreno",
                phonenumber: "45566565"
            },
            adminContact: {
                email: "psaravia@gmail.com",
                phonenumber: "454545",
                lastname: "Saravia",
                name: "Pedro"
            },
            scheduleFrom: new Date(0, 0, 1, 12, 57, 0),
            scheduleTo: new Date(0, 0, 1, 0, 30, 0),
            address: "Calle 44 nro 2365",
            specialties: "Chivitos",
            description: "Chiviteria",
            phone: "5646465",
            name: "El Galpon de Jose"
        }];
 
        $scope.tableParams = new NgTableParams({sorting: {name: 'asc'}}, { dataset: dataList});


    }]);